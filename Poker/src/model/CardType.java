package model;

import java.util.Vector;

public enum CardType 
{
	ACE ("Ace"),
	TWO("Two"),
	THREE("Three"),
	FOUR("Four"),
	FIVE("Five"),
	SIX("Six"),
	SEVEN("Seven"),
	EIGHT("Eight"),
	NINE("Nine"),
	TEN("Ten"),
	JACK("Jack"),
	QUEEN("Queen"),
	KING("King");

	private final String myName;

	CardType(String name) 
	{
		myName = name;
	}

	public String getCardType() 
	{
		return null;
	}

}