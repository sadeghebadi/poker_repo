package model;

public enum CardSuit 
{
	HEARTS ("Hearts"),
	DIAMONDS ("Diamonds"),
	CLUBS ("Clubs"),
	SPADES ("Spades");
	
	private String mySuit;

	CardSuit(String name) 
	{
		mySuit = name;
	}

	public String getSuit() 
	{
		return null;
	}

}