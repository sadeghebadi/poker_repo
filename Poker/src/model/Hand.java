package model;

import java.util.Vector;

public class Hand
{
	private Vector <Card> myCards;
	private int myHandScore;
	private final static int HAND_SIZE = 5;
	
	public Hand() 
	{
	    
	}
	//Shouldn't we have two methods here? 
	//Taking a vector doesn't make sense because remove either takes a Int or Object
	public void discard(Vector<Integer> position) 
	{
		for(int i = 0; i < position.capacity(); i++)
		{
			myCards.remove(position.get(i));
		}
	}

	private int rateHand() 
	{
		/*
		/so that we are all on the same page I will write what hands should return what score for testing purposes.
		/there are three numbers that are represented, the first is the type of hand (ex three of a kind) and that is multiplied by 10000.
		/the second is there to differentiate between equal kinds of hands (ex a pair of 2s and a pair of 3s this number is to make it known that the 3s win)
		/and it will be multiplied by 100. The third number is the high card (ex both have pair of 2s so one with high card wins) and it isn't multiplied.
		/The number that is returned is the addition of these 3 numbers. I will list the hand types and their associated values:
		/highcard 0, one pair 1, two pair 2, three of a kind 3, straight 4, flush 5, full house 6, four of a kind 7, straight flush 8
		/the second number is going to be a value from 2 to 14 based on the highest card involved in the combination 
		/(ex pair of 5s and pair of 2s the number will be 5, ex2 pair of 3s the number is 3) 
		/full house is slightly different because the important part is the 3 of a kind.
		/the high card is pretty self explanatory and should be a value between 2 and 14.
		*/
		return 0;
	}

	public int getHandScore() 
	{
		return myHandScore;
	}

	public String toString() 
	{
		return null;
	}
	
	public void receiveCard(Card card) 
	{
	    
	}
	
	public int compareTo(Hand hand)
	{
        return 0;
	}

}