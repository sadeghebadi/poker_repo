package testmodel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Player;
import model.PokerEngine;


public class PokerEngineTests
{
    private PokerEngine engine;
    
    @Before
    public void setup()
    {
        engine = new PokerEngine();
    }
    
    
    /**
     * Tests if engine changes turn when changeTurn() is called.
     * 
     * @author Jacob Hell and Michael Clay
     */
    @Test
    public void testChangeTurn() 
    {
       	/*boolean isTurn = true;
    	Player player;
    	PokerEngine engine;
    	player = new Player("John");
    	engine = new PokerEngine();
    	*/
    	engine.createGame(2);
    	Player[] players = engine.getPlayers();
    	players[0].setTurn(true);
    	
        assertTrue("Should be this player1's turn", players[0].isTurn());
        assertFalse("Should not be player2's turn", players[1].isTurn());
 
    	engine.changeTurn();
        assertFalse("Should not be this player1's turn", players[0].isTurn());
        assertTrue("Should be player2's turn", players[1].isTurn());
        
        engine.changeTurn();
        assertFalse("Should not be this player2's turn", players[1].isTurn());
        assertTrue("Should be player1's turn", players[0].isTurn());
   }
    
    /**
     * Tests if poker engine will create more players than allowed.
     * 
     * @author George Robbins
     */
    @Test
    public void testExceedPlayerLimit()
    {   	
    	boolean works = false;
    	engine.createGame(10);
    	Player[] players = new Player[10];
    	players = engine.getPlayers();
    	if( players.length <= PokerEngine.MAX_NUM_PLAYERS )
    		works = true;
    	assertTrue("Passed a number > MAX_NUM_PLAYERS: should not create that many players", works);
   }
    
    /**
     * Tests if poker engine creates players.
     * 
     * @author George Robbins
     */
    @Test
    public void testCreatePlayers()
    {
    	engine.createGame(3);
    	Player[] players = engine.getPlayers();
    	assertTrue("Created 3 players", players.length == 3);
    }
    
    @Test
    public void testCreateGame()
    {
        engine.createGame(5);
        for (int i = 0; i < engine.getPlayers().length-1; i++)
        {
            assertTrue("A Player should have been created along with the game", engine.getPlayers()[i] != null);
            assertTrue("The object created by the createGame method should be an instance of the Player Class", 
                    engine.getPlayers()[i] instanceof Player);
        }
    }
    
    /**
     * Tests poker engine deals each player cards.
     * 
     * @author George Robbins
     */
    @Test
    public void testDealCards()
    {
    	engine.createGame(5);
    	Player[] players = engine.getPlayers();
    	engine.dealCards();
    	
    	for( Player p : players)
    	{
    		model.Hand playerHand = p.getHand();
    		assertTrue("Players should have cards", playerHand.getHandScore() >= 0 );
    	}
    }
    
    /**
     * Tests whether or not resetGame keeps the original players.
     * 
     * @author Marisa Gomez
     */
    @Test
    public void testResetGamePlayers()
    {
        Player[] players;
        Player[] playersReset;
        boolean equal = true;

        engine.createGame(5);
        players = engine.getPlayers();
        engine.resetGame();
        playersReset = engine.getPlayers();

        for (int i = 0; i < players.length; i++)
        {
            if (players[i] != playersReset[i])
            {
                equal = false;
            }
        }

        assertTrue("Players should be the same.", equal);
    }
    
    /**
     * Tests whether or not resetAll keeps the original players.
     * 
     * @author Marisa Gomez
     */
    @Test
    public void testResetAllPlayers()
    {
        Player[] players;
        Player[] playersReset;
        boolean equal = false;
        
        engine.createGame(5);
        players = engine.getPlayers();
        engine.resetAll();
        playersReset = engine.getPlayers();
        //the new game doesn't necessarily have the same number of players
        for (int i = 0; i < players.length && i < playersReset.length; i++)
        {
            if (players[i] == playersReset[i])
            {
                equal = true;
            }
        }

        assertFalse("Players should not be the same.", equal);
    }
}
