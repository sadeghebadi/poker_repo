/**
 * Test Enumeration Card Type
 */
package testmodel;

import static org.junit.Assert.*;
import org.junit.Test;
import model.CardType;

public class CardTypeTests
{

	/**
	 * Test if type was set correctly
	 */
    @Test
    public void testGetCardType()
    {
        CardType cardType;
        cardType = CardType.ACE;

        assertTrue("Type is wrong", cardType.getCardType() == CardType.ACE.getCardType());

    }

}
