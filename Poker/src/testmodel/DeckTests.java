/**
 * Test the Deck class
 */
package testmodel;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import model.*;

public class DeckTests
{
	private Deck myDeck;

	/**
	 * Setups a deck that all test cases will use
	 */
	@Before
    public void setup()
    {
    	// do stuff
		myDeck = new Deck(); 
    }
    /**
     * Test to make sure no cards a duplicated
     */
    @Test
    public void testDeckUniqueSimple() //QKW20140326 can I get the same card twice?
    {
    	java.util.HashSet<Card> mhs = new java.util.HashSet<Card>();
    	Card card;
    	boolean unique;
    	while (true)
    	{
    		card = myDeck.draw();
    		if (card == null)
    				break;
    		unique = mhs.add(card);
    		assertTrue("duplicate card", unique);
    	}
    	
    }
    /**
     * Test to see if shuffle creates a unique deck
	 * QKW20140402 everyone is failing this, there is a real question about whether or not it is a valid requirement, commenting it out at least temporarily
    @Test
    public void testDeckUniqueWithShuffle() //QKW20140326 can I get the same card twice if I shuffle when I shouldn't?
    {
    	java.util.HashSet<Card> mhs = new java.util.HashSet<Card>();
    	Card card;
    	while (true)
    	{
    		myDeck.shuffle();
    		card = myDeck.draw();
    		if (card == null)
    				break;
    		assertFalse("duplicate card", mhs.add(card));
    	}
    }
     */
    
    /**
     * Tests if the draw() method actually draws a card
     * Date: 03/27/2014 
     * @author: JDalmasso
     */
    @Test
	public void testDraw() 
	{
		Card card;
		boolean valid;
		card = myDeck.draw();
		if(card == null)
		{
			valid = false;
		}
		else
		{
			valid = true;
		}
		assertTrue("We should have drawn a card", valid);
	}
    
    /**
     * Tests if each card drawn is unique, and dissimilar from the previous card.
     */
    @Test
    public void testNextCardDifferent()
    {
        Card card;
        card = myDeck.draw();
        assertFalse("Each card drawn should be unique, not the same as the last one", card == myDeck.draw());
    }

    /**
     * Tests if the deck is shuffled properly by checking if the first four cards in the deck were the same both times.
     */
    @Test
    public void testIfShuffles()
    {
        Card[] cards = new Card[4];
        for (int i = 0; i < 3; i++)
        {
            cards[i] = myDeck.draw();
        }
        myDeck.shuffle();
        int count = 0;
        for(int i = 0; i < 4; i++)
        {
            if (myDeck.draw() == cards[i])
            {
                count++;
            }
        }
        assertFalse("The deck was improperly shuffled.", count >= 4);

    }
}

