/**
 * Test the card class
 */
package testmodel;

import static org.junit.Assert.*;
import model.Card;
import model.CardSuit;
import model.CardType;

import org.junit.Test;

public class CardTests {
    
	/**
	 * Test suit of card
	 */
	@Test
	public void testCardSuit() 
	{
		Card card = new Card(CardSuit.HEARTS, CardType.ACE);
		assertTrue("Suit is wrong", card.getSuit() == CardSuit.HEARTS.getSuit());
	}
	
	/**
	 * Test to make sure card was created properly 
	 */
	@Test
    public void testCardType() /* Added by Michael */
    {
        Card card = new Card(CardSuit.HEARTS, CardType.ACE);
        assertTrue("Type is wrong", card.getType() == CardType.ACE.getCardType());
    }


}
